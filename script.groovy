def buildapp(){
		echo 'building the application'
		sh 'mvn package'
	
}
def buildImage(){
	echo 'building the dokcer image'
	withCredentials([usernamePassword(credentialsId: 'dockerhub-id', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
	    sh 'docker build -t bipinkhatariya/my-repo:jma-1.7 .'
	    sh "echo $PASS | docker login -u $USER --password-stdin"
		sh 'docker push bipinkhatariya/my-repo:jma-1.7'
     }
} 

def deployApp() {
	
    echo "deploying the application from ${BRANCH_NAME}"
} 

return this
